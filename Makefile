vcard: vcard.c
	afl-gcc -Wall -Wextra -Werror -ggdb -O0 $^ -o $@ \
	   `pkg-config --cflags --libs glib-2.0 libebook-contacts-1.2`

fuzz: vcard
	afl-fuzz -i test-cases -o findings -m 500MB ./$^ @@

clean:
	rm -f vcard

.PHONY: fuzz clean
