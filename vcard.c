/* Public domain. */
/* afl-gcc -Wall -Wextra -Werror -ggdb -O0 vcard.c -o vcard \
 *    `pkg-config --cflags --libs glib-2.0 libebook-contacts-1.2` */

#include <libebook-contacts/libebook-contacts.h>
#include <glib.h>
#include <string.h>

static void
parse_vcard_file (const gchar *vcard_filename)
{
  EVCard *vcard = NULL;
  gchar *contents = NULL;
  gsize length;
  GError *error = NULL;

  g_file_get_contents (vcard_filename, &contents, &length, &error);
  g_assert_no_error (error);

  vcard = e_vcard_new_from_string (contents);
  e_vcard_get_attributes (vcard);

  g_clear_object (&vcard);
  g_free (contents);
  g_clear_error (&error);
}

int
main (int argc,
      char *argv[])
{
  guint i;

  for (i = 1; i < (unsigned int) argc; i++)
    {
      parse_vcard_file (argv[i]);
    }
}
